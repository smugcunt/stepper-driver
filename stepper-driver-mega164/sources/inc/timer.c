/*
 * init_timer.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: void
 */

#include	"timer.h"

/*****************************************************************/
/**						DEFINES SECTION							**/
/*****************************************************************/
//

/*****************************************************************/
/**					8-BIT TIMERS SECTION						**/
/*****************************************************************/
void	Timer0CounterInit (uint16_t prescaler, uint8_t counter)
{
	// timer setup function
	// receives counter and prescaler for timer

	TCCR0B |= (1 << WGM02);	// set timer CTC mode

	// check that prescaler is within bounds
	if (prescaler >= 1024)
		TCCR0B |= (1 << CS02) | (1 << CS00);
	else if (prescaler >= 256)
		TCCR0B |= (1 << CS02);
	else if (prescaler >= 64)
		TCCR0B |= (1 << CS01) | (1 << CS00);
	else if (prescaler >= 8)
		TCCR0B |= (1 << CS01);
	else
		TCCR0B |= (1 << CS00);

	OCR0A = counter - 1;	// set timer counter
}
/*****************************************************************/
void	Timer2CounterInit (uint16_t prescaler, uint8_t counter)
{
	// timer setup function
	// receives counter and prescaler for timer

	TCCR2B |= (1 << WGM22);	// set timer CTC mode

	// check that prescaler is within bounds
	if (prescaler >= 1024)
		TCCR2B |= (1 << CS02) | (1 << CS00);
	else if (prescaler >= 256)
		TCCR2B |= (1 << CS02);
	else if (prescaler >= 64)
		TCCR2B |= (1 << CS01) | (1 << CS00);
	else if (prescaler >= 8)
		TCCR2B |= (1 << CS01);
	else
		TCCR2B |= (1 << CS00);

	OCR2A = counter - 1;	// set timer counter
}
/*****************************************************************/
/**					16-BIT TIMERS SECTION						**/
/*****************************************************************/
void	Timer1CounterInit (uint16_t prescaler, uint16_t counter)
{
	// timer setup function
	// receives counter and prescaler for timer

	TCCR1B |= (1 << WGM12);	// set timer CTC mode

	// set prescaler within its bounds
	if (prescaler >= 1024)
		TCCR1B |= (1 << CS12) | (1 << CS10);
	else if (prescaler >= 256)
		TCCR1B |= (1 << CS12);
	else if (prescaler >= 64)
		TCCR1B |= (1 << CS11) | (1 << CS10);
	else if (prescaler >= 8)
		TCCR1B |= (1 << CS11);
	else
		TCCR1B |= (1 << CS10);

//	OCR1A = counter;	// set timer counter
	OCR1A = counter - 1;	// set timer counter
}

/*****************************************************************/
/**						TIMERS FUNCTIONS						**/
/*****************************************************************/
void	TimerRemoveInt (uint8_t number, uint8_t mask)
{
	// gets timer number to attach interrupt

	switch (number)
	{
		case 1:
			TIMSK1 &= ~mask;
			break;

		case 2:
			TIMSK2 &= ~mask;
			break;

		default:
			TIMSK0 &= ~mask;
			break;
	}
}
/*****************************************************************/
void	TimerAttachInt (uint8_t number, uint8_t mask)
{
	// gets timer number to attach interrupt

	switch (number)
	{
		case 1:
			TIMSK1 |= mask;
			break;

		case 2:
			TIMSK2 |= mask;
			break;

		default:
			TIMSK0 |= mask;
			break;
	}
}
/*****************************************************************/
uint16_t	TimerGetCount (uint8_t number)
{
	// receives timer number
	// outputs current timer count

	uint16_t count;
	uint8_t	sreg;

	sreg = SREG;
	cli();

	// get different count depending on timer number
	switch (number)
	{
		case 1:
			count = (TCNT1H << 8) + TCNT1L;
			break;

		case 2:
			count = TCNT2;
			break;

		default:
			count = TCNT0;
			break;
	}
	SREG = sreg;

	return	count;
}
/*****************************************************************/
void	TimerSetCompareRegister (uint8_t number, uint16_t count)
{
	// sets timer compare output register
	// receives timer number

	uint8_t	sreg;

	sreg = SREG;	// save status register
	cli();	// disable int

	// get different count depending on timer number
	switch (number)
	{
		case 1:
			OCR1A = count;
			break;

		case 2:
			OCR2A = count & 0xFF;	// make sure only first byte is written
			break;

		default:
			OCR0A = count & 0xFF;	// make sure only first byte is written
			break;
	}
	SREG = sreg;	// return normal status register
}
/*****************************************************************/
void	TimerResetCount (uint8_t number)
{
	// resets timer counter
	// gets timer number

	uint8_t	sreg;

	sreg = SREG;
	cli();

	// get different count depending on timer number
	switch (number)
	{
		case 1:
			TCNT1H = TCNT1L = 0;
			break;

		case 2:
			TCNT2 = 0;
			break;

		default:
			TCNT0 = 0;
			break;
	}
	SREG = sreg;
}
/*****************************************************************/


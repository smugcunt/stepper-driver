/*
 * init_uart.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: void
 */

//#define		(__AVR_ATmega164A__)
#include	"comm.h"
/*****************************************************************/
/**						UART INIT SECTION						**/
/*****************************************************************/
void	UART0Init (uint32_t speed)
{
	// initialize uart 0
	// gets uart speed

#define	BAUDRATE	((F_CPU) / ((speed)*16UL) - 1)

	// sets desired uart div (to get baudrate)
	UBRR0H = (BAUDRATE >> 8); // get 8..15 bits of desired speed
	UBRR0L = BAUDRATE;	// get 0..7 bits of desired speed

	// enables receiver and transmitter
	UCSR0B = (1 << TXEN0) | (1 << RXEN0);

	// set 8 bit data format
	UCSR0C &= ~( (1 << UMSEL00) | (1 << UMSEL01) );
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
}

/*****************************************************************/
/**					UART FUNCTION SECTION						**/
/*****************************************************************/
void	UART0SendChar (char symbol)
{
	// transmits single symbol through set uart
	// receives single char symbol

	// wait while transmission is in progress
	while (! (UCSR0A & (1 << UDRE0))) ;

	UDR0 = symbol;	// write symbol
}
/*****************************************************************/
uint8_t	UART0SendString (char *buffer)
{
	// transmits buffer through uart
	// receives buffer, not more than 250 symbols!
	// returns transmitted buffer size

	uint8_t count;

	count = 0;
	while ((*buffer != '\0') && (count++ < UART_BUFFER_LEN))
	{
		UART0SendChar(*buffer++);
	}

	return	count;
}
/*****************************************************************/
char	UART0GetChar (void)
{
	// gets incoming char from uart
	// returns back received char

	// wait while receive is in progress
	while (!(UCSR0A) & (1 << RXC0)) ;

	return	UDR0;
}
/*****************************************************************/
uint8_t	UART0GetString (char *buffer)
{
	// reads incoming string buffer from uart
	// receives buffer array to fill
	// returns received buffer size < 250

	char	symbol;
	uint8_t count;

	count = 0;
	// while symbol != EOL, receive string
	symbol = UART0GetChar();
	while ((symbol != '\r') && (symbol != '\n') \
			&& (count++ < UART_BUFFER_LEN) )
	{
		*buffer++ = symbol;
		symbol = UART0GetChar();
	}
	*buffer = '\0';

	return	count;
}



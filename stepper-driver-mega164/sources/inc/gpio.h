/*
 * gpio.h
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

#ifndef SOURCES_INC_GPIO_H_
#define SOURCES_INC_GPIO_H_
/*****************************************************************/
#include	<stdlib.h>
#include	<inttypes.h>

#include	<avr/io.h>
#include	<avr/interrupt.h>
#include	<avr/sleep.h>
#include	<util/delay.h>

#include	"enum.h"

/*****************************************************************/
void	GPIOInit (uint8_t port, uint8_t pin, uint8_t dir);
void	GPIOAttachExtInt (uint8_t pin, uint8_t mask);
void	GPIOAttachPCInt (uint8_t pin);

void	GPIOWrite (uint8_t port, uint8_t pin, uint8_t val);
uint8_t	GPIORead (uint8_t port, uint8_t pin);

/*****************************************************************/
#endif /* SOURCES_INC_GPIO_H_ */


################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../revA/inc/init_gpio.c \
../revA/inc/init_timer.c \
../revA/inc/init_uart.c \
../revA/inc/system.c 

OBJS += \
./revA/inc/init_gpio.o \
./revA/inc/init_timer.o \
./revA/inc/init_uart.o \
./revA/inc/system.o 

C_DEPS += \
./revA/inc/init_gpio.d \
./revA/inc/init_timer.d \
./revA/inc/init_uart.d \
./revA/inc/system.d 


# Each subdirectory must supply rules for building sources it contributes
revA/inc/%.o: ../revA/inc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega164p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



/*
 * unem.h
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

/*****************************************************************/
#ifndef SOURCES_INC_ENUM_H_
#define SOURCES_INC_ENUM_H_
/*****************************************************************/
enum	{ INPUT, OUTPUT };
enum	{ INT_LOW, INT_ANY, INT_FALL, INT_RISE };
enum	{ GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF };
enum	{ OFF, ON };
/*****************************************************************/
#endif /* SOURCES_INC_ENUM_H_ */

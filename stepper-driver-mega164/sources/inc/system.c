/*
 * system.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: void
 */

#include	"system.h"

/*****************************************************************/
/**					CLOCK MANIPULATION SECTION					**/
/*****************************************************************/
uint16_t	SystemClockSetPrescaler (uint16_t prescaler)
{
	// sets system clock prescaler
	// gets prescaler (1..256)
	// returns set prescaler

	uint16_t	result;

	switch (prescaler)
	{
		case 1:
			CLKPR = (1 << CLKPCE);
			CLKPR &= ~( (1<<CLKPS3) | (1<<CLKPS2) | (1<<CLKPS1) | (1<<CLKPS0) );
			result = 1;
			break;

		case 2:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS0);
			result = 2;
			break;

		case 4:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS1);
			result = 4;
			break;

		case 8:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS1) | (1<<CLKPS0);
			result = 8;
			break;

		case 16:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS2);
			result = 16;
			break;

		case 32:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS2) | (1<<CLKPS0);
			result = 32;
			break;

		case 64:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS2) | (1<<CLKPS1);
			result = 64;
			break;

		case 128:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS2) | (1<<CLKPS1) | (1<<CLKPS0);
			result = 128;
			break;

		case 256:
			CLKPR = (1 << CLKPCE);
			CLKPR |= (1<<CLKPS3);
			result = 256;
			break;

		default:
			// assert ();
			result = 0;
			break;
	}
	return	result;
}
/*****************************************************************/
uint8_t	SystemGetResetVector (void)
{
	// accesses mcu status register
	// returns register contents

	uint8_t	result;

	result = MCUSR & 0b00011111;

	return	result;
}
/*****************************************************************/
/**						WATCHDOG SECTION						**/
/*****************************************************************/
void	WatchdogDisable (void)
{
	// disables watchdog timer

	cli();//__disable_interrupt();
	wdt_reset(); //wdt_reset();

	// reset watchdog reset vector in status register
	MCUSR &= ~(1 << WDRF);

	// enable changes in watchdog status register
	WDTCSR |= (1 << WDCE) | (1 << WDE);
	WDTCSR = 0x00;	// turn off watchdog (set to reset state)

	sei();//__enable_interrupt();
}
/*****************************************************************/
void	WatchdogReset (void)
{
	// simply reset watchdog timer

	cli();
	wdt_reset();

	// enable changes in watchdog status register
	WDTCSR |= (1 << WDCE) | (1 << WDE);

	sei();
}

void	WatchdogEnable (uint16_t prescaler)
{
	// gets prescaler (2K..1024K)
	// set and reset watchdog timer
	// reset time = 1000*prescaler/128K [ms]

	cli();
	wdt_reset();

	// enable changes in watchdog status register
	WDTCSR |= (1<<WDCE) | (1<<WDE);

	// set new prescaler
	if (prescaler <= 2)			// 16 ms
		WDTCSR = (1<<WDE);
	else if (prescaler <= 4)	// 32 ms
		WDTCSR = (1<<WDE) | (1<<WDP0);
	else if (prescaler <= 8)	// 64 ms
		WDTCSR = (1<<WDE) | (1<<WDP1);
	else if (prescaler <= 16)	// 125 ms
		WDTCSR = (1<<WDE) | (1<<WDP1) | (1<<WDP0);
	else if (prescaler <= 32)	// 250 ms
		WDTCSR = (1<<WDE) | (1<<WDP2);
	else if (prescaler <= 64)	// 500 ms
		WDTCSR = (1<<WDE) | (1<<WDP2) | (1<<WDP0);
	else if (prescaler <= 128)	// 1000 ms
		WDTCSR = (1<<WDE) | (1<<WDP2) | (1<<WDP1);
	else if (prescaler <= 256)	// 2000 ms
		WDTCSR = (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1<<WDP0);
	else if (prescaler <= 512)	// 4000 ms
		WDTCSR = (1<<WDE) | (1<<WDP3);
	else //if (prescaler <= 1024)	// 8000 ms
		WDTCSR = (1<<WDE) | (1<<WDP3) | (1<<WDP0);

	sei();
}
/*****************************************************************/


/*
 * comm.h
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

#ifndef SOURCES_INC_COMM_H_
#define SOURCES_INC_COMM_H_
/*****************************************************************/
#include	<stdlib.h>
#include	<inttypes.h>

#include	<avr/io.h>
#include	<avr/interrupt.h>
#include	<avr/sleep.h>
#include	<util/delay.h>
/*****************************************************************/
#endif /* SOURCES_INC_COMM_H_ */

/*****************************************************************/
#define		UART_BUFFER_LEN		250	// max uart buffer length
/*****************************************************************/
void	UART0Init (uint32_t speed);
uint8_t	UART0SendString (char *buffer);
char	UART0GetChar (void);
uint8_t	UART0GetString (char *buffer);

//void	I2CInit ();

/*****************************************************************/


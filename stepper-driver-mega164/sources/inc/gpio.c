/*
 * init_gpio.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: void
 */

#include	"gpio.h"	//

/*****************************************************************/
/**						GPIOs  SECTION							**/
/*****************************************************************/
void	GPIOInit (uint8_t port, uint8_t pin, uint8_t dir)
{
	// sets pin/pins as input/output
	// gets port, pin number and direction

	switch (port)
	{
		case GPIOA:
			DDRA = (dir > 0)? DDRA | pin : DDRA & (~pin);
			break;

		case GPIOB:
			DDRB = (dir > 0)? DDRB | pin : DDRB & (~pin);
			break;

		case GPIOC:
			DDRC = (dir > 0)? DDRC | pin : DDRC & (~pin);
			break;

		case GPIOD:
			DDRD = (dir > 0)? DDRD | pin : DDRD & (~pin);
			break;

		default:
			//#assert ("IMPPOSIBLE PORT");
			break;
	}
}
/*****************************************************************/
void	GPIOAttachExtInt (uint8_t pin, uint8_t mask)
{
	// attaches external interrupt (INT0..2)
	// receives external interrupt mask

	uint8_t	int_mask, int_pin;

	// out of bounds check
	int_pin = (pin > 2)? 2 : pin;
	int_mask = (mask > 3)? 3 : mask;

	// set INT.. interrupt bit
	EIMSK |= (1 << pin);
	EICRA |= mask << (2 * pin);	// rising/falling edge
	sei();
}
/*****************************************************************/
void	GPIOAttachPCInt (uint8_t pin)
{
	// attaches PC interrupt (PCINT0..31)
	// receives external interrupt mask

	uint8_t	int_pin;

	// out of bounds check
	int_pin = (pin > 31)? 31 : pin;

	// set corresponding pc int register
	if (int_pin >= 24)
	{
		PCICR |= (1 << PCIE3);
		PCMSK3 |= (1 << (int_pin - 24));
	}
	else if (int_pin >= 16)
	{
		PCICR |= (1 << PCIE2);
		PCMSK2 |= (1 << (int_pin - 16));
	}
	else if (int_pin >= 8)
	{
		PCICR |= (1 << PCIE1);
		PCMSK1 |= (1 << (int_pin - 8));
	}
	else //if (int_pin >= 0)
	{
		PCICR |= (1 << PCIE0);
		PCMSK0 |= (1 << (int_pin));
	}

	sei();
}
/*****************************************************************/
void	GPIOWrite (uint8_t port, uint8_t pin, uint8_t val)
{
	// writes pin/pins to output register
	// gets port, pin number and value to write

	switch (port)
	{
		case GPIOA:
			PORTA = (val > 0)? PORTA | pin : PORTA & (~pin);
			break;

		case GPIOB:
			PORTB = (val > 0)? PORTB | pin : PORTB & (~pin);
			break;

		case GPIOC:
			PORTC = (val > 0)? PORTC | pin : PORTC & (~pin);
			break;

		case GPIOD:
			PORTD = (val > 0) ? PORTD | pin : PORTD & (~pin);
			break;

		default:
			//#assert ("IMPPOSIBLE PORT");
			break;
	}
}

/*****************************************************************/
uint8_t	GPIORead (uint8_t port, uint8_t pin)
{
	// reads pin/pins from input register
	// gets port, pin number
	// returns read val from register

	uint8_t	val;

	switch (port)
	{
		case GPIOA:
			val = PINA & pin;
			return val;
			break;

		case GPIOB:
			val = PINB & pin;
			return val;
			break;

		case GPIOC:
			val = PINC & pin;
			return val;
			break;

		case GPIOD:
			val = PIND & pin;
			return val;
			break;

		default:
			//#assert ("IMPPOSIBLE PORT");
			break;
	}
	return	0;
}

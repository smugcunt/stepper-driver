/*****************************************************************/
#ifndef		F_CPU
#define		F_CPU	16000000UL	// define default cpu quartz
#endif

//#define	(__AVR_ATmega164P__)	1	// define used mcu
/*****************************************************************/
//#include	<avr/io.h>
//#include	<avr/interrupt.h>
//#include	<avr/sleep.h>

#include	"inc/enum.h"
#include	"inc/comm.h"
#include	"inc/gpio.h"
#include	"inc/system.h"
#include	"inc/timer.h"

/*****************************************************************/
/**						PIN MACRO  SECTION						**/
/*****************************************************************/
#define	PIN_CUR1		0	// dip switch current mode pa0
#define	PIN_CUR2		1	// dip switch current mode pa1
#define	PIN_CUR3		2	// dip switch current mode pa2

#define	PIN_MOD1		5	// dip switch control mode pd7
#define	PIN_MOD2		6	// dip switch control mode pd6
#define	PIN_MOD3		7	// dip switch control mode pd5

#define PIN_LED_WORK	0	// green "working" led pb0
#define PIN_LED_WARN	1	// red "warning" led pb1

#define PIN_CLK			2	// driver clock input pd2
#define PIN_DIR			3	// driver direction input pd3
#define PIN_EN			4	// driver enable input pd4

#define	PIN_EP1			2	// end point 1 input pb2
#define	PIN_EP2			3	// end point 2 input pb3

#define PIN_I2C_SCL		0	// I2C clock output pc0
#define PIN_I2C_SDA		1	// I2C data in/out pc1

#define PIN_485_DIR		4	// 485 direction output pb4
#define PIN_485_RX		0	// 485 receive input pd0
#define PIN_485_TX		1	// 485 transmit output pd1

#define	PIN_A1			7	// driver "A1" output pc7
#define	PIN_AEN			6	// driver "AEN" output pc6
#define	PIN_A2			5	// driver "A2" output pc5

#define	PIN_B1			2	// driver "B1" output pc2
#define	PIN_BEN			3	// driver "BEN" output pc3
#define	PIN_B2			4	// driver "B2" output pc4

#define	PIN_ISEN1		4	// driver "A" side current input pa4
#define	PIN_ISEN2		5	// driver "B" side current input pa5

/*****************************************************************/
/**					PERIPHERIAL VALUES SECTION					**/
/*****************************************************************/
#define	ADC_PRESCALER	25	// // f_adc = f_cpu / (PRESCALER * 16)
#define	ADC_VREF		5000	// current vref voltage (mV)
//#define	UART_SPEED		19200	// default uart speed
//#define	I2C_SPEED		50000	// default i2c speed

#define	CURR_MULT		(100)		// current multiplier value
#define	CURR_OFFSET		(CURR_MULT)	// offset (100, 200..700, 800)
#define	CURR_SHUNT		(220)		// shunt resistance [mOhm]

#define	LED_OFF			1	// pin output logical level to switch off led
#define	LED_ON			0	// pin output logical level to switch on led

#define	MCU_MIN_VOLTAGE	1100	// mcu minimal voltage [mV]
#define	MCU_MAX_VOLTAGE	6000	// mcu maximum voltage [mV]

#define	PIN_EP_ON_LVL	1	// end point level considered 'on'
#define	PIN_CTRL_ON_LVL	0	// control pins level consider 'on'

/*****************************************************************/
/**					VARIABLE MACRO SECTION						**/
/*****************************************************************/
#define	DRIVE_NORM		0	// set by power mode dip switch pins
#define	DRIVE_FAST		1	// speed x 2
#define	DRIVE_WAVE		2	// wave driver move
#define	DRIVE_HALF		3	// half step mode

#define	ERROR_END1		1
#define	ERROR_END2		2
#define	ERROR_CUR_OVF	8

#define	STATE_DISABLED	0	// driver disabled state
#define	STATE_STOPPED	1
#define	STATE_NORMAL	2
#define	STATE_REVERSE	4	// reverses driver control pins

#define	TIMER_TICKS		(250)	// timer init compare value (=1ms)
#define	TIMER_WD_RESET	1000	// ticks before watchdog reset [ms]
#define	TIMER_LED_WARN	5000	// ticks before switching off warning led
#define	TIMER_LAST_TICK	(TIMER_LED_WARN)	// last timer tick
#define	TIMER_MAX_TICKS	65500	// maximum timer tick delay

#define	OUTPUT_SIZE		8	// number of driver output stages

/*****************************************************************/


/*
 * adc.c
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

#include	"adc.h"
/*****************************************************************/
/**						ADC INIT SECTION						**/
/*****************************************************************/
void	ADCInit (uint8_t prescaler)
{
	// initialize adc at set pin
	// receives pin number and prescaler

	// set default aref pin as reference
	//ADMUX &= ~( (1 << REFS0) | (1 << REFS1) );
	// set aref the same as avcc reference
	ADMUX = (1 << REFS0);

	// set adc clock prescaler [16..160 kHz for 1..10kSamples]
	//ADCSRA |= (1 << ADPS0) | (1 << ADPS1) | (ADPS2);
	ADCSRA &= ~( (1 << ADPS0) | (1 << ADPS1) | (ADPS2) );
	if (prescaler <= 2)
		ADCSRA |= (1 << ADPS0);
	else if (prescaler <= 4)
		ADCSRA |= (1 << ADPS1);
	else if (prescaler <= 8)
		ADCSRA |= (1 << ADPS1) | (1 << ADPS0);
	else if (prescaler <= 16)
		ADCSRA |= (1 << ADPS2);
	else if (prescaler <= 32)
		ADCSRA |= (1 << ADPS2) | (1 << ADPS0);
	else if (prescaler <= 64)
		ADCSRA |= (1 << ADPS2) | (1 << ADPS1);
	else if (prescaler <= 128)
			ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	else
		;	// nothing
}

/*****************************************************************/
/**					ADC FUNCTION SECTION						**/
/*****************************************************************/
void	ADCEnable (void)
{
	// simply enable adc
	ADCSRA |= (1 << ADEN);	// set adc enable bit

	ADCSRA |= (1 << ADSC);	// start first dummy conversion
	while (! (ADCSRA & (1 << ADSC)) );	// wait for conv end
}
/*****************************************************************/
void	ADCDisable (void)
{
	// simply disable adc (also terminates current conversion)
	ADCSRA &= ~(1 << ADEN);	// clear adc enable bit
	while (ADCSRA & (1 << ADEN) );	// wait a moment
}
/*****************************************************************/
void	ADCPinsDigitalDisable (uint8_t mask)
{
	// used register to disable digital pins on adc port
	// gets pin mask

	DIDR0 = mask;
}

/*****************************************************************/
uint16_t	ADCConvert (uint8_t channel)
{
	// completes one 10-bits adc conversion
	// returns 16 bits raw adc value

	uint16_t	result;

	ADMUX &= ~(0b0111);	// clear lowest 3 bits
	ADMUX |= (channel & 0b0111);	// set channel 0..7

	ADCSRA |= (1 << ADSC);	// start first dummy conversion
	while (! (ADCSRA & (1 << ADSC)) );	// wait for conv end

	result = (ADCH << 8) + ADCL;

	return	result;
}
/*****************************************************************/
uint8_t	ADCConvert8 (uint8_t channel)
{
	// requires left adjust for adc results
	// completes one 8-bits adc conversion
	// returns 8 bits raw adc value

	uint8_t	result;

	ADMUX &= ~(0b0111);	// clear lowest 3 bits
	ADMUX |= (channel & 0b0111);	// set channel 0..7

	ADCSRA |= (1 << ADSC);	// start first dummy conversion
	while (! (ADCSRA & (1 << ADSC)) );	// wait for conv end

	result = ADCH;

	return	result;

}
/*****************************************************************/

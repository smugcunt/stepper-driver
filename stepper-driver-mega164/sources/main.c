/*
 * init.c
 *
 *	Created on: 12 oct. 2018 �.
 *	Author: PRazum
 */
/*****************************************************************/
//#include	<assert.h>
#include	<stdlib.h>
#include	<inttypes.h>

#include	"main.h"

/*****************************************************************/
/**						GLOBAL VARIABLES						**/
/*****************************************************************/
//volatile uint16_t	gTick;	// timer tick value
volatile uint16_t	gCurrent;	// prints current in mA
volatile uint16_t	gTimerTick;	// global main timer ticks
uint16_t	gCurrentMax;	// max allowable current value (mV)

volatile uint8_t	gPwmTick = 1;	// pwm like timer presets
volatile uint8_t	gError = 0;	// global errors [if > 0 then :(]
volatile uint8_t	gOutputStage = 0;	// current output stage

uint8_t	gResetState;	// mcu reset vector
uint8_t	gOutput[OUTPUT_SIZE];	// driver port output timed schema

#if (defined (UART_SPEED) )
char	buffer[80];	// uart input/output buffer
#endif

/*****************************************************************/
/**				FUNCTION PROTOTYPES AND MACROS					**/
/*****************************************************************/
int8_t	init_system (void);
int8_t	init_gpio (void);
int8_t	init_power (void);

uint16_t	ConvertToCurrent (uint16_t val);

uint8_t	SetDriverMode (uint8_t *input);
int16_t	SetCurrentMax (uint16_t mult, int16_t offset);

/*****************************************************************/
// macro for quick led switching
#define		__led_work(AA)	{GPIOWrite(GPIOB, PIN_LED_WORK, AA);}
#define		__led_warn(AA)	{GPIOWrite(GPIOB, PIN_LED_WARN, AA);}
//void	GPIOWrite (uint8_t port, uint8_t pin, uint8_t val)
// macro for bulk driver port control
#define		__drive_out(MM)	{PINC = MM;}
// macro for pin-by-pin driver control
#define		__drive_a1(AA)	{GPIOWrite(GPIOC, PIN_A1, AA);}
#define		__drive_a2(AA)	{GPIOWrite(GPIOC, PIN_A2, AA);}
#define		__drive_b1(AA)	{GPIOWrite(GPIOC, PIN_B1, AA);}
#define		__drive_b2(AA)	{GPIOWrite(GPIOC, PIN_B2, AA);}
#define		__drive_aen(AA)	{GPIOWrite(GPIOC, PIN_AEN, AA);}
#define		__drive_ben(AA)	{GPIOWrite(GPIOC, PIN_BEN, AA);}
// macro for rs 485 direction switch
#define		__rs485_dir(AA)	{GPIOWrite(GPIOB, PIN_485_DIR, AA);}

/*****************************************************************/
/**						MAIN FUNCTION INPUT						**/
/*****************************************************************/
int	main (void)
{
	init_system();
	init_gpio();
	init_power();

	while (1) ;

	return	0x00;	// EXIT_SUCCESS
}
/*****************************************************************/
/**						MAIN INIT SECTION						**/
/*****************************************************************/
int8_t	init_system (void)
{
	// init default system peripherial: clock, timer, watchdog, uart
	// returns number of completed steps

	int8_t	step;

	step = 0;
	// get previous reset vector (interesting bits 1..3)
	gResetState = (SystemGetResetVector() & 0b00001110 );

	step = 1;
	// init watchdog timer with 2 seconds delay
	WatchdogEnable(256);	// 256K / 128K

	step =2;
	// init timer wit set prescaler and counter
	Timer1CounterInit(64, TIMER_TICKS);	// tick every 1ms
	TimerAttachInt(1, (1 << OCIE1A) );

	step = 3;
	// init 'pwm' hardware timers, don't attach interrupt
	Timer0CounterInit(8, 40);	// 50 kHz timer, tick 20us
	Timer2CounterInit(8, gPwmTick);	// 50 kHz timer, tick 20us

#if (defined (UART_SPEED) )
	// init rs-485 communication pins
	step = 12;
	GPIOInit(GPIOB, (1 << PIN_485_DIR), OUTPUT);
	UART0Init(UART_SPEED);
#endif

#if (defined (I2C_SPEED) )
	// init i2c communication
	step = 13;
	//I2CInit(I2C_SPEED);
#endif

	return	step;
}
/*****************************************************************/
int8_t	init_gpio (void)
{
	// initialize default gpio pins
	// returns number of completed steps

	int8_t	step;

	step = 0;
	// init end point inputs and attach interrupt
	GPIOInit(GPIOB, (1 << PIN_EP1) | (1 << PIN_EP2), INPUT);
	GPIOAttachPCInt(10);	// pc int happens on every pin change
	GPIOAttachPCInt(11);	// attached both ep1 and ep2 pins

	step = 1;
	// init led output pins
	GPIOInit(GPIOB, (1 << PIN_LED_WORK)|(1 << PIN_LED_WARN), OUTPUT);
	__led_work(LED_OFF);
	__led_warn(LED_ON);

	step = 2;
	// init driver 'A' and 'B' coil pins
	GPIOInit(GPIOC, (1 << PIN_A1)|(1 << PIN_AEN)|(1 << PIN_A2), OUTPUT);
	GPIOInit(GPIOC, (1 << PIN_B1)|(1 << PIN_BEN)|(1 << PIN_B2), OUTPUT);

	step = 3;
	// init driver mode dip switch input pins
	GPIOInit(GPIOD, (1 << PIN_MOD1)|(1 << PIN_MOD2)|(1 << PIN_MOD3), INPUT);

	step = 4;
	// init current mode dip switch input pins
	GPIOInit(GPIOA, (1 << PIN_CUR1)|(1 << PIN_CUR2)|(1 << PIN_CUR3), INPUT);

	return	step;
}
/*****************************************************************/
int8_t	init_power (void)
{
	// initialize power components: adc, interrupt period

	uint16_t	current;
	uint8_t		step, temp;

	step = 0;
	// set main drive mode
	temp = SetDriverMode(gOutput);

	step = 1;
	// init driver control inputs
	GPIOInit(GPIOD, (1 << PIN_CLK)|(1 << PIN_DIR)|(1 << PIN_EN), INPUT);
	GPIOAttachExtInt(1, INT_ANY);	// direction attached on any change
	GPIOAttachExtInt(0, INT_LOW);	// clock attached on low level
	GPIOAttachPCInt(28);	// attached driver enable pc int

	step = 2;
	// set maximum allowable current
	current = (SetCurrentMax (CURR_MULT, CURR_OFFSET) & 1023);
	//gCurrentMax = (current & 1023) * (temp & PIN_MOD3);
	gCurrentMax = ((temp & PIN_MOD3) > 0 )? 2 * current : current;

	step = 3;
	// init adc at set speed
	ADCInit(ADC_PRESCALER);	// adc_clk = f_cpu / (N * 16)
	// disable neighbor pins for improved signal to noise ratio
	ADCPinsDigitalDisable(0b1111 << (PIN_ISEN1 - 1));
	ADCEnable();	// init adc routine (one dummy conversion)

	return	step;	// exit success
}

/*****************************************************************/
/**					PERIPHERIAL ROUTINE SECTION					**/
/*****************************************************************/
uint8_t	SetDriverMode (uint8_t *input)
{
	// gets input array to set according to mode dip switch
	// output read value (0..7)

	uint8_t	result;

	result = PIND & ((1 << PIN_MOD1) | (1 << PIN_MOD2) | (1 << PIN_MOD3) );
	result >>= PIN_MOD1;	// shift data right to get 0..7 values

	// set driver control routine
	switch ((result & 0b0011) )	// get first two bits of dip switch pins
	{
		case DRIVE_FAST:	// double the normal drive speed
			input[0] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[1] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[2] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[3] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[4] = input[0];
			input[5] = input[1];	// they are the same
			input[6] = input[2];	// since we are using fast mode
			input[7] = input[3];
			//GPIOAttachExtInt(0, INT_ANY);	// clock attached on any change
			break;

		case DRIVE_WAVE:	// wave mode
			input[0] = (1<<PIN_BEN) | (1<<PIN_B2);
			input[1] = (1<<PIN_BEN) | (1<<PIN_B2);
			input[2] = (1<<PIN_A1) | (1<<PIN_AEN);
			input[3] = (1<<PIN_A1) | (1<<PIN_AEN);
			input[4] = (1<<PIN_B1) | (1<<PIN_BEN);
			input[5] = (1<<PIN_B1) | (1<<PIN_BEN);
			input[6] = (1<<PIN_AEN) | (1<<PIN_A2);
			input[7] = (1<<PIN_AEN) | (1<<PIN_A2);
			break;

		case DRIVE_HALF:	// half step mode
			input[0] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[1] = (1<<PIN_BEN) | (1<<PIN_B2);
			input[2] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[3] = (1<<PIN_A1) | (1<<PIN_AEN);
			input[4] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[5] = (1<<PIN_B1) | (1<<PIN_BEN);
			input[6] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[7] = (1<<PIN_AEN) | (1<<PIN_A2);

			break;

		case DRIVE_NORM:
		default:	// normal driver mode
			input[0] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[1] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[2] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[3] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_BEN) | (1<<PIN_B2);
			input[4] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[5] = (1<<PIN_A1) | (1<<PIN_AEN) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[6] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_B1) | (1<<PIN_BEN);
			input[7] = (1<<PIN_AEN) | (1<<PIN_A2) | (1<<PIN_B1) | (1<<PIN_BEN);
			break;
	}

	return	(result & 0b0111);
}
/*****************************************************************/
void	__arr8_reserve (uint8_t *arr, uint8_t size)
{
	// reverse contents of array using temp array
	uint8_t	temp[size];
	uint8_t	i;
	
	// create temp array
	for (i = 0; i < size; i++)
		temp[i] = arr[size - 1 - i];
	
	// reverse contents of old array
	for (i = 0; i < size; i++)
		arr[i] = temp[i];
	
	free(temp);
}
/*****************************************************************/
void	SetDriverState (uint8_t state)
{
	// gets state to put driver into

	switch (state)
	{
		case STATE_DISABLED:	// 'manual' driver mode
			__drive_aen(0);	// switch off driver shutdown pins
			__drive_ben(0);
			break;

		case STATE_STOPPED:	// state 
			
			// disable interrupt on
			break;

		case STATE_REVERSE:	// driver reversed mode
			__led_warn(LED_OFF);
			__led_work(LED_ON);
			__drive_aen(1);	// switch on driver shutdown pins
			__drive_ben(1);
			// reverse driver output timed schema
			__arr8_reserve(gOutput, OUTPUT_SIZE);
			break;

		case STATE_NORMAL:	// driver normal mode
		default:
			// normal operation state
			__led_warn(LED_OFF);
			__led_work(LED_ON);
			__drive_aen(1);	// switch on driver shutdown pins
			__drive_ben(1);
			break;
	}
}
/*****************************************************************/
int16_t	SetCurrentMax (uint16_t mult, int16_t offset)
{
	// reads current max pins
	// returns maximum adc value to consider current overflow

	uint16_t	result;

	result = PINA & ((1 << PIN_CUR1) | (1 << PIN_CUR2) | (1 << PIN_CUR3) );
	result >>= PIN_CUR1;	// shift data right to get 0..7 values
	result *= (mult > 0)? (mult & 1023) : 1;	// multiply by input value

	return	(result + offset);	// returns read current
}
/*****************************************************************/
/**					ADDITIONAL FUNCTION SECTION					**/
/*****************************************************************/
uint16_t	ConvertToCurrent (uint16_t val)
{
	// gets reference voltage (mV) and channel number (0..7)
	// returns transformed single adc conversion

	uint32_t	temp;

#if	((ADC_VREF > MCU_MIN_VOLTAGE) && (ADC_VREF < MCU_MAX_VOLTAGE) )
	// get conversion result, transform to current [mA]
	temp = (ADC_VREF * val ) / CURR_SHUNT;
	temp = (1000 * temp) / 1024;

	return	(uint16_t)temp;
#else
	#error ("ADC_VREF is not within MCU Minimal and Maximum voltages");
	return	65535;	// return impossible vref error number
#endif
}
/*****************************************************************/
//void	PinLedSwitch (uint8_t pin, int8_t state)
//{
//	// switches (xor/off/on) set led (0..1)
//	// gets led number and state to switch
//
//	uint8_t	int_pin;
//
//	int_pin = pin & ((PIN_LED_WORK) | (PIN_LED_WARN) );
//
//	if (state > 0)
//		PINB &= ~(1 << int_pin);
//	else if (state == 0)
//		PINB |= (1 << int_pin);
//	else //if (state < 0)
//		PINB ^= (1 << int_pin);
//}
/*****************************************************************/
/*****************************************************************/
/**					PIN CHANGE ISR SECTION						**/
/*****************************************************************/
ISR (PCINT1_vect)	// happens when pcint 8..15 pins get changed
{
	// end point signal change interrupt
	// get signal levels and stop contraption if necessary

	uint8_t	pin1, pin2;

	pin1 = (PINB & (1 << PIN_EP1) ) >> PIN_EP1;
	pin2 = (PINB & (1 << PIN_EP2) ) >> PIN_EP2;

	// match end point signal level with known macro value
	if (pin1 == PIN_EP_ON_LVL)
	{
		__led_warn(LED_ON);
		gError |= ERROR_END1;	// add errors to register
		SetDriverState(STATE_STOPPED);
	}
	else if (pin2 == PIN_EP_ON_LVL)
	{
		__led_warn(LED_ON);
		gError |= ERROR_END2;	// add errors to register
		SetDriverState(STATE_STOPPED);
	}
	// else
	// {
	//	clear error level
		// gError &= ~( (1 << ERROR_END1) | (1 << ERROR_END2) );
		// SetDriverState(STATE_NORMAL);
	// }
}
/*****************************************************************/
ISR (PCINT3_vect)	// happens when pcint 23..31 pins get changed
{
	// check if driver enable interrupt received

	uint8_t	pin;

	pin = (PIND & (1 << PIN_EN) ) >> PIN_EN;
	if (pin == PIN_CTRL_ON_LVL)
	{
		// disable driver 'hold' - 'manual' state
		__led_work(LED_OFF);
		__led_warn(LED_ON);
		SetDriverState(STATE_DISABLED);
	}
	else
	{
		// enable driver 'hold' - normal state
		__led_work(LED_ON);
		__led_warn(LED_OFF);
		SetDriverState(STATE_NORMAL);
	}
}
/*****************************************************************/
ISR (INT0_vect)	// happens when int0 pin gets triggered
{
	// driver clock proceeds next output driver port stage
	// compares power shunt value with current maximum

	// bulk driver port output
	__drive_out(gOutput[gOutputStage]);
	gOutputStage++;
}
/*****************************************************************/
ISR (INT1_vect)	// happens when int1 pin gets triggered
{
	// driver direction changes driver port output timed schema

	gOutputStage = 0;
	SetDriverState(STATE_REVERSE);
}
/*****************************************************************/
/**						TIMER ISR SECTION						**/
/*****************************************************************/
ISR (TIMER0_COMPA_vect)	// happens on timer compare A match
{
	// start of 'pwm' routine
	// set timer 2 interrupt and reset counter
	// xor current driver output
	
	TimerRemoveInt(0, (1 << OCIE0A) );	// remove int from this timer
	TimerSetCompareRegister(2, gPwmTick);// set new compare value
	TimerResetCount(2);					// reset 2nd timer count
	TimerAttachInt(2, (1 << OCIE2A) );	// attach int to other timer
}
/*****************************************************************/
ISR	(TIMER2_COMPA_vect)	// happens on timer compare A match
{
	// middle part of 'pwm' routine
	
	TimerRemoveInt(2, (1 << OCIE0A) );	// remove int from this timer
	TimerResetCount(0);					// reset 0th timer count
	TimerAttachInt(0, (1 << OCIE2A) );	// attach int to other timer
}
/*****************************************************************/
ISR (TIMER1_COMPA_vect)	// happens on timer compare A match
{
	// timer responsible for watchdog reset, warning led switch off
	// checking and comparing current to maximum allowable current

	uint16_t	a, b;

	// check adc values
	a = ADCConvert(PIN_ISEN1);
	b = ADCConvert(PIN_ISEN2);
	// transform the one which is greater
	gCurrent = ConvertToCurrent((a > b)? a : b);
	// check if current overflow happened
	if (gCurrent > gCurrentMax)
	{
		__led_warn(LED_ON);		// turn on warning led
		gPwmTick++;				// decrease pwm width
		gError = ERROR_CUR_OVF;	// add error to the stack
		TimerAttachInt(0, (1 << OCIE0A) );	// turn on timer ints
		//TimerAttachInt(2, (1 << OCIE2A) );
	}
	else if (gError & ERROR_CUR_OVF)
	{
		if (gPwmTick > 1)
			gPwmTick--;
		else
		{
			TimerRemoveInt(0, (1 << OCIE0A) );	// remove timer ints
			TimerRemoveInt(2, (1 << OCIE2A) );
		}
	}

	gTimerTick++;
	if (gTimerTick % TIMER_WD_RESET == 0)
	{
		// reset watchdog counter
		WatchdogReset();
	}
	else if ((gTimerTick % TIMER_LED_WARN == 0) && (!gError) )
	{
		// switch off warning led if no error are found
		__led_warn(LED_OFF);
		// set new max current based on dip switch settings
		gCurrentMax = (SetCurrentMax (CURR_MULT, CURR_OFFSET) & 1023);
	}

	// if timer tick count reached max > drop to zero
	if (gTimerTick >= TIMER_LAST_TICK)
		gTimerTick = 0;
}
/*****************************************************************/

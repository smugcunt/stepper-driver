/*
 * adc.h
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

#ifndef SOURCES_INC_ADC_H_
#define SOURCES_INC_ADC_H_
/*****************************************************************/
#include	<stdlib.h>
#include	<inttypes.h>

#include	<avr/io.h>
#include	<avr/interrupt.h>
#include	<avr/sleep.h>
#include	<util/delay.h>

/*****************************************************************/
void	ADCInit (uint8_t prescaler);
void	ADCEnable (void);
void	ADCDisable (void);
void	ADCPinsDigitalDisable (uint8_t mask);

uint16_t	ADCConvert (uint8_t channel);
uint8_t	ADCConvert8 (uint8_t channel);

/*****************************************************************/
#endif /* SOURCES_INC_ADC_H_ */

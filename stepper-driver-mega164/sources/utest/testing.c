/*
 * testing.c
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

/*****************************************************************/
/**						UNIT TESTING							**/
/*****************************************************************/
#include	"../lib/unity/unity.h"
#include	"../lib/unity/unity.c"

#include	"testing.h"

#include	"../inc/comm.h"

/*****************************************************************/
/**							UART TESTS							**/
/*****************************************************************/
void setUp(void)
{
	//This is run before EACH TEST
//	Counter = 0x5a5a;
}

void tearDown(void)
{
}

void test1 (void)
{
	//All of these should pass
	TEST_ASSERT_EQUAL(0, UART0SendString(""));
	TEST_ASSERT_EQUAL(1, UART0SendString(" "));
	TEST_ASSERT_EQUAL(1, UART0SendString("  "));
}

void test2 (void)
{
	//This should be true because setUp set this up for us before this test
//	TEST_ASSERT_EQUAL_HEX(0x5a5a, FunctionWhichReturnsLocalVariable());
}

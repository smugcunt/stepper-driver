################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sources/inc/adc.c \
../sources/inc/comm.c \
../sources/inc/gpio.c \
../sources/inc/system.c \
../sources/inc/timer.c 

OBJS += \
./sources/inc/adc.o \
./sources/inc/comm.o \
./sources/inc/gpio.o \
./sources/inc/system.o \
./sources/inc/timer.o 

C_DEPS += \
./sources/inc/adc.d \
./sources/inc/comm.d \
./sources/inc/gpio.d \
./sources/inc/system.d \
./sources/inc/timer.d 


# Each subdirectory must supply rules for building sources it contributes
sources/inc/%.o: ../sources/inc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=c99 -funsigned-char -funsigned-bitfields -mmcu=atmega164p -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



/*
 * timer.h
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: void
 */

#ifndef SOURCES_INC_TIMER_H_
#define SOURCES_INC_TIMER_H_
/*****************************************************************/
#include	<stdlib.h>
#include	<inttypes.h>

#include	<avr/io.h>
#include	<avr/interrupt.h>
#include	<avr/sleep.h>
#include	<util/delay.h>

//#include	"../default.h"

/*****************************************************************/
#endif /* SOURCES_INC_TIMER_H_ */

/*****************************************************************/
void	Timer0CounterInit (uint16_t prescaler, uint8_t counter);
void	Timer2CounterInit (uint16_t prescaler, uint8_t counter);
void	Timer1CounterInit (uint16_t prescaler, uint16_t counter);
void	TimerAttachInt (uint8_t number, uint8_t mask);
void	TimerRemoveInt (uint8_t number, uint8_t mask);

void	TimerSetCompareRegister (uint8_t number, uint16_t count);
uint16_t	TimerGetCount (uint8_t number);
void	TimerResetCount (uint8_t number);

/*****************************************************************/
